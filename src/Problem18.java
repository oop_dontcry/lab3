import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        int type;
        System.out.print("Please select star type [1-4,5 is Exit]: ");
        type = sc.nextInt();
        
        if(type == 1) {
            System.out.print("Please input number: ");
            n = sc.nextInt();
            for(int i = 1; i <= n; i++) {
                for(int j = 0; j < i; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        } else if(type == 2) {
            System.out.print("Please input number: ");
            n = sc.nextInt();
            for(int i = n; i > 0; i--) {
                for(int j = 0; j < i; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        } else if(type == 3) {
            System.out.print("Please input number: ");
            n = sc.nextInt();
            for(int i = 0; i < n; i++) {
                for(int j = 0; j < i; j++) {
                    System.out.print(" ");
                }
                for(int j = 0; j < n - i ; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        } else if(type == 4) {
            System.out.print("Please input number: ");
            n = sc.nextInt();
            for(int i = 4; i >= 0; i--) {
                for(int j = 0; j < n; j++) {
                    if(j >= i) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        } else if(type == 5) {
            System.out.println("Bye bye!!!");
            System.exit(0);
        } else {
            System.out.println("Error: Please input number between 1-5");
        }

    }
}